//
// Created by user on 21.02.2022.
//

#ifndef PAMCHECKUSER_H
#define PAMCHECKUSER_H

struct pamcheckuser_data {
    char *user;
    char *password;
};

/**************************************************
 * @brief check user credentials with PAM
 * @param[in] user username
 * @param[in] password password
 * @return 0 - OK, 1 - FAILED
 **************************************************/
int auth_user(const char *user, const char *password);

#endif //PAMCHECKUSER_H
