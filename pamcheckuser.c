#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <security/pam_appl.h>
#include "pamcheckuser.h"

/**************************************************
 * @brief callback of PAM conversation
 * @param[in] num_msg the count of message
 * @param[in] msg PAM message
 * @param[out] resp our response
 * @param[in] appdata_ptr custom data passed by struct pam_conv.appdata_ptr
 * @return state
 **************************************************/
static int conversation(int num_msg, const struct pam_message **msg, struct pam_response **resp, void *appdata_ptr)
{
    /* retrieve user credentials struct */
    struct pamcheckuser_data *user_data = (struct pamcheckuser_data*)(appdata_ptr);

    int i;

    /* check the count of message */
    if (num_msg <= 0 || num_msg >= PAM_MAX_MSG_SIZE)
    {
        fprintf(stderr, "invalid num_msg(%d)\n", num_msg);
        return PAM_CONV_ERR;
    }

    /* alloc memory for response */
    if ((*resp = malloc(num_msg * sizeof(struct pam_response))) == NULL)
    {
        fprintf(stderr, "bad alloc\n");
        return PAM_BUF_ERR;
    }

    /* response for message */
    for (i = 0; i < num_msg; i++)
    {
        const struct pam_message *m = msg[i];
        struct pam_response *r = resp[i];

        r->resp_retcode = 0;    /* currently un-used, zero expected */

        switch (m->msg_style)
        {
        case PAM_PROMPT_ECHO_OFF:   /* get the input with echo off, like the password */
            r->resp = user_data->password;
            break;

        case PAM_PROMPT_ECHO_ON:    /* get the input with echo on, like the username */
            r->resp = user_data->user;
            break;

        case PAM_TEXT_INFO:         /* normal info */
            printf("%s\n", m->msg);
            break;

        case PAM_ERROR_MSG:         /* error info */
            fprintf(stderr, "%s\n", m->msg);
            break;

        default:
            fprintf(stderr, "unexpected msg_style: %d\n", m->msg_style);
            break;
        }
    }
    return PAM_SUCCESS;
}

/**************************************************
 * @brief check user credentials with PAM
 * @param[in] user username
 * @param[in] password password
 * @return 0 - OK, 1 - FAILED
 **************************************************/
int auth_user(const char *user, const char *password)
{
    struct pamcheckuser_data *user_data = malloc(sizeof(struct pamcheckuser_data));
    user_data->user = strdup(user);
    user_data->password = strdup(password);

    struct pam_conv pam_conv = {
            conversation,
            user_data};
    pam_handle_t *pamh;

    if (PAM_SUCCESS != pam_start("check", NULL, &pam_conv, &pamh))
    {
        fprintf(stderr, "pam_start failed\n");
        return EXIT_FAILURE;
    }

    if (PAM_SUCCESS != pam_authenticate(pamh, 0))
    {
        fprintf(stderr, "pam_authenticate failed\n");
        pam_end(pamh, 0);
        return EXIT_FAILURE;
    }

    if (PAM_SUCCESS != pam_acct_mgmt(pamh, 0))
    {
        fprintf(stderr, "pam_acct_mgmt failed\n");
        pam_end(pamh, 0);
        return EXIT_FAILURE;
    }

    pam_end(pamh, 0);
    printf("pam_authenticate ok");
    return EXIT_SUCCESS;
}

