#include <stdio.h>
#include <stdlib.h>
#include "pamcheckuser.h"

int main(int argc, char *argv[])
{
    char *user;
    char *password;

    if(argc == 3) {
        user = argv[1];
        password = argv[2];
    }
    else
    {
        fprintf(stderr, "Usage: pamcheckuser [username] [password]\n");
        return EXIT_FAILURE;
    }

    return auth_user(user, password);
}
